
pub mod ls {
use std::fs::read_dir;

pub fn run_ls(args : &[&str]) {
    match args.len() {
        1 => print_dir_contents(&vec!["."]),
        _ => print_dir_contents(&args[1..])
    }
}

fn print_dir_contents(args : &[&str]) {
    for arg in args {
        let entries = match read_dir(arg) {
            Ok(x) => x,
            Err(e) => panic!("Error reading dir '{}': {}", arg, e)
        };
        for entry in entries {
            let p = entry.unwrap().path();
            println!("{}", p.to_str().unwrap_or(""));
        }
    }
}
}

pub mod echo {

pub fn run_echo(args: &[&str]) {
    if args.len() > 1 {
        for arg in &args[1..] {
            print!("{} ", *arg);
        }
        println!("");
    }
}

}
