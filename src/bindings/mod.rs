mod keys {
    pub fn insert_text(string: &str) {
    }
    pub fn delete_text(from: i32, to: i32) {
    }
    pub fn replace_text(string: &str, from: i32, to: i32) {
    }

    pub enum Commands{
        SetMark,
        BeginningOfLine,
        BackwardChar,
        CommandFunc,
        Delete,
        EndOfLine,
        ForwardChar,
        Abort,
        Rubout,
        Complete,
        Newline,
        KillLine,
        ClearScree,
        GetNextHistory,
        GetPreviousHistory,
        QuotedInsert,
        ReverseSearchHistory,
        ForwardSearchHistory,
        TransposeChars,
        UnixLineDiscard,
        QuotedInsert,
        UnixWordRubout,
        Yank,
        CharSearch,
        UndoCommand,
        Insert,
        BackwardsKillWord,
        ForwardsKillWord,
        TabInsert,
        RevertLine,
        YankNthArg


    }
}
