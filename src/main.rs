#![feature(libc)]
use std::io::{Write};
use std::ffi::CString;
use nix::unistd::{fork, execve, Fork};
use nix::sys::wait::{wait, WaitStatus};
use nix::Error;
use nix::errno::Errno;
extern crate libc;
extern crate mio;
extern crate nix;
extern crate term;

//https://github.com/jankes/tetris1/blob/master/tetris1.rs
// Ideas: 
// * export scripts to sh
// * gobject and xpcom loading.
// * filters to transmit typed data between processes. (only structural typing?)
// * remiplement some shell builtins s.t. they use typed data.
//   * Maybe they use xpcom
// * Work with COM on windows; AppleScript on OS X.
// 

mod builtins;

fn run_cmd(input : &str) {
    match fork() {
        Ok(Fork::Parent(_)) => {
            match wait() {
                Ok(status) => match status {
                    WaitStatus::Exited(pid) => println!("Parent: reaped child: {}", pid),
                    WaitStatus::StillAlive => println!("Parent: Child still alive")
                },
                Err(e) => panic!(e)
            }
        }
        Ok(Fork::Child) => {
            let split_input : Vec<&str> = input.split_whitespace().collect();
            if split_input.len() > 0 {
                let cmd = CString::new(split_input[0]).unwrap();
                let args : Vec<CString> = split_input.iter().map(|x| {CString::new(*x).unwrap()}).collect();
                let path = ::std::env::var("PATH").unwrap_or("".to_owned());
                let env = [CString::new(format!("PATH={}", path)).unwrap()];
                println!("Path: {:?}", env);
                //let env = [CString::new("PATH=.").unwrap()];
                match nix::unistd::execve(&cmd, &args, &env) {
                    Ok(_) => unreachable!(),
                    Err(Error::InvalidPath) => println!("Could not run command '{}': Command not found", split_input[0]),
                    Err(Error::Sys(errno)) => println!("Could not run command '{}': {}", split_input[0], errno.desc()),
                }
            }
        }
        Err(e) => panic!(e)
    }
}

fn handle_input(input: &str) -> bool {
    let split_input : Vec<&str> = input.split_whitespace().collect();

    if split_input.len() == 0 {
        // may as well clean children.
        match wait() {
                Ok(status) => match status {
                    WaitStatus::Exited(pid) => println!("Parent: reaped child: {}", pid),
                    WaitStatus::StillAlive => println!("Parent: Child still alive")
                },
                Err(Error::Sys(Errno::ECHILD)) => (),
                Err(Error::Sys(errno)) => println!("Error reaping processes: {}", errno.desc()),
                Err(Error::InvalidPath) => println!("Error reaping processes: Invalid path(?)")
        }
        return true;
    }
    let mut should_continue = true;
    match split_input[0] {
        "ls" => builtins::ls::run_ls(&split_input),
        "echo" => builtins::echo::run_echo(&split_input),
        "exit\n" => should_continue = false,
        "logout\n" => should_continue = false,
        _ => run_cmd(input)
    }
    return should_continue;
}

fn main() {
    println!("WARNING: HACKING!");
    let prompt = "> ";
    let mut stdin = ::std::io::stdin();
    let mut should_continue = true;
    while should_continue{
        let mut t = term::stdout().unwrap();
        t.fg(term::color::RED).unwrap();
        write!(t, "{}", prompt).unwrap();
        t.reset().unwrap();
        t.flush().unwrap();
        let mut input : String = "".to_owned();
        match stdin.read_line(&mut input) {
            Ok(_) => should_continue = handle_input(input.trim_right_matches('\n')),
            Err(e) => panic!("HACKING ERROR: {}", e)
        }
    }
    println!("HACKING COMPLETE!");
}
